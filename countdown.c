///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 06c - Countdown - EE 205 - Spr 2022
/// @file countdown.cpp
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////




#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<unistd.h> 

#define REFYEAR (2022-1900)
#define REFMONTH (2-1)
#define REFDAY (21)
#define REFHOUR (13)
#define REFMIN (21)
#define REFSEC (28)


int main() {

   time_t reference; //referecne time in seconds
   time_t current; //current time in seconds
   char refTime[80]; //character array to print ref time
   struct tm ref; //ref time struct to hard code into program
   int diffTime;  //difference between ref and current time
   int years, days, hours, minutes, seconds; 

  /*fil in reference time structure*/
   ref.tm_sec = REFSEC;
   ref.tm_min = REFMIN;
   ref.tm_hour = REFHOUR;
   ref.tm_mday = REFDAY;
   ref.tm_mon = REFMONTH;
   ref.tm_year = REFYEAR;
   ref.tm_isdst = -1;


   /*print ref time*/
   reference = mktime(&ref);
   strftime(refTime, sizeof(ref), "%a %b %d %X %p %Z %Y", &ref);
   printf("Reference time: %s \n", refTime);
   
  /*get the current time into a struct*/
   time_t now = time(0);
   struct tm currentTime = *localtime(&now);
   current = mktime(&currentTime);
   
   

  int i = 1; 
  while (i != 0){ //while true statement
   

   /*update the time*/
   time_t now = time(0);
   struct tm currentTime = *localtime(&now);
   current = mktime(&currentTime);
   

   /*difference between ref time and current time in seconds*/
   if (current > reference){
   diffTime = difftime(current, reference);
   }

   else{
   diffTime = difftime(reference, current);
   }
  

     /*convert the difference in time in seconds to years, days, months*/
     years = (diffTime/31536000);
     days = (((diffTime- (years*31536000))/86400));
     hours = ((diffTime-(years*31536000+days*86400))/3600);
     minutes = ((diffTime-years*31536000-days*86400-hours*3600)/60);
     seconds =  (diffTime - ((years*31536000)+(days*86400)+(hours*3600)+(minutes*60)));


     /*print the time diff*/ 
     printf("Years: %d, Days: %d, Hours: %d, Minutes: %d, Seconds: %d\n", years, days, hours, minutes, seconds); 

     /*wait one second before reconverting*/
     sleep(1);
  }

    return 0;
} 
